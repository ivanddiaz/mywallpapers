#!/bin/bash
# https://help.gnome.org/admin/system-admin-guide/3.8/backgrounds-extra.html.en
echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd">'
echo '<wallpapers>'

for WALLPAPER in *.jpg; do
    echo '  <wallpaper deleted="false">'
    echo '    <name>'"$WALLPAPER"'</name>'
    echo '    <filename>'"/usr/share/backgrounds/$WALLPAPER"'</filename>'
    echo '  </wallpaper>'
done

echo '</wallpapers>'
